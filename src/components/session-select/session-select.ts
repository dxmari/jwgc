import { Component } from '@angular/core';
import { NavController } from 'ionic-angular/navigation/nav-controller';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { ViewController } from 'ionic-angular/navigation/view-controller';

@Component({
  selector: 'session-select',
  templateUrl: 'session-select.html'
})
export class SessionSelectComponent {

  sessionList = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public view: ViewController) {
    this.sessionList = navParams.get('options');
    console.log(this.sessionList)
  }

  register(session){
    this.view.dismiss(session);
  }

  cancel(){
    this.view.dismiss(false);
  }

}
