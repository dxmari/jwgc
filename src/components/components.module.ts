import { NgModule } from '@angular/core';
import { SessionSelectComponent } from './session-select/session-select';
@NgModule({
	declarations: [SessionSelectComponent],
	imports: [],
	exports: [SessionSelectComponent]
})
export class ComponentsModule {}
