import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CommonService } from '../../app/services/index';

@IonicPage({
  name: 'notifications',
  segment: 'notifications'
})
@Component({
  selector: 'page-notifications',
  templateUrl: 'notifications.html',
})
export class NotificationsPage {
  notificationsList = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, public common: CommonService) {

  }

  ionViewDidLoad() {
    var params = '?tag=getNotifications&member_id=' + this.common.authData.member_id;
    this.common.postService(params).then((res: any) => {
      this.notificationsList = res;
      this.notificationsList.forEach(repo => {
        repo.date = new Date(repo.sent_on + ' '+ repo.sent_at).toJSON();
      });
      console.log(this.notificationsList);
    })
  }

}
