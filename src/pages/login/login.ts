import { HomePage } from './../home/home';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CommonService, StorageService } from '../../app/services/index';
@IonicPage({
  name: 'login'
})
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  loginFlg = true;
  otp = '';
  logoutFlg = 'hide';
  otpFlg = false;
  memFlg = false;
  mobilFlg = false;
  response :any = {};
  constructor(public navCtrl: NavController, public navParams: NavParams, public common: CommonService, public storage: StorageService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  login(userid, mobile) {
    if(userid && mobile){
      this.mobilFlg = false;
      this.memFlg = false;
      var params = '?tag=login&member_id=' + userid + '&mobile_no=' + mobile
      this.common.postService(params).then((res: any) => {
        console.log(res);
        if(res.error == 0){
          this.loginFlg = false;
          this.otp = res.OTP;
          this.response = res;
        }else{
          this.common.presentToast('Oops, Something went wrong.');
        }
      }).catch(err => {
        console.log(err);
      })
    }else{
      if(!userid){ this.memFlg = true};
      if(!mobile) {this.mobilFlg = true};
    }
  }

  checkOTP(otp) {
    if(otp){
      if (this.otp == otp) {
        this.otpFlg = false;
        this.common.authData = this.response;
        localStorage.setItem('auth', JSON.stringify(this.response));
        this.storage.setObject('auth', this.response);
        this.navCtrl.setRoot(HomePage);
      } else {
        this.common.presentToast('Invalid OTP or expired')
      }
    }else{
      if(!otp) this.otpFlg = true;
    }
  }
}
