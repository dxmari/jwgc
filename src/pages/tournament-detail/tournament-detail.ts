import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CommonService } from '../../app/services/index';
import { SessionSelectComponent } from '../../components/session-select/session-select';

@IonicPage({
  name: 'tournament-detail',
  segment: 'tournament-detail'
})
@Component({
  selector: 'page-tournament-detail',
  templateUrl: 'tournament-detail.html',
})
export class TournamentDetailPage {
  tournament: any = {};
  imagUrl = '';
  sessionList = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, public common: CommonService) {
    this.imagUrl = common.IMGURL;
    this.tournament = navParams.get('params');
    console.log(this.tournament)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TournamentDetailPage');
    var params = '?tag=getSessions&tournament=' + this.tournament.tournament + '&tour_year=' + this.tournament.tour_year;
    this.common.postService(params).then((res: any) => {
      this.sessionList = res;
    })

    var params = '?tag=getRegistrationStatus&tournament=' + this.tournament.tournament + '&tour_year=' + this.tournament.tour_year + "&mem_id=" + this.common.authData.member_id;
    this.common.postService(params).then((res: any) => {
      console.log(res);
      if (res.error == 1) {

      } else {

      }
    })
  }

  openRegister() {
    this.common.ionicPopup(SessionSelectComponent, { options: this.sessionList }).then(res => {
      if (res) {
        console.log(res);
        this.bookTournament(res);
      }
    });
  }

  bookTournament(slot) {
    var params = "?tag=registerTournament&slno=" + slot.slno + "&mem_id=" + this.common.authData.member_id + "&isWaitingList=false&session_type=" + slot.session_name + "&session_date=" + slot.session_date + " &session_time=" + slot.session_time;
    this.common.postService(params).then((res: any) => {
      console.log(res);
      if (res.error == 0) {
        this.common.presentToast('Tournament Booked successfully');
      }
    })
  }

  cancelBook(slot) {
    var params = "?tag=unregisterTournament&slno=" + slot.slno + "&mem_id=" + this.common.authData.member_id;
    this.common.postService(params).then((res: any) => {
      console.log(res);
      if (res.error == 0) {
        this.common.presentToast('Tournament unregistered successfully.');
      }
    })
  }
}
