import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CommonService } from './../../app/services/global.service';

@IonicPage({
  name: 'rewards'
})
@Component({
  selector: 'page-rewards',
  templateUrl: 'rewards.html',
})
export class RewardsPage {
  tournamentsList = [];
  imagUrl = '';
  constructor(public navCtrl: NavController, public navParams: NavParams, public common: CommonService) {
    this.imagUrl = common.IMGURL;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RewardsPage');
    var params = '?tag=getCompletedTournaments';
    this.common.postService(params).then((res: any) => {
      this.tournamentsList = res;
    })
  }

}
