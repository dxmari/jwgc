import { Platform } from 'ionic-angular';
import { StorageService } from './../../app/services/storage.service';
import { HomePage } from './../home/home';
import { CommonService } from './../../app/services/global.service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage({
  segment: 'scores',
  name: 'scores'
})
@Component({
  selector: 'page-score-card',
  templateUrl: 'score-card.html',
})
export class ScoreCardPage {
  totalScoreCard = [];
  score = 0;
  currGolfer = "0";
  currHole = "0";
  golfers = [];
  holes = [];
  finalScore = [];
  players = '';
  currRow: any;
  currCol = 1;
  currSlot: any = {}//{ "booked_member_name": "Naveen Kumar K M", "slot_id": 64, "club": "Jayachamaraja Wadiyar Golf Club", "hc": 10, "error": 0, "booked_member_id": "NK 0011", "64": [{ "member_id": "TJ 0015", "club": "Jayachamaraja Wadiyar Golf Club", "member_name": "Tejaswini" }, { "member_id": "DS 0012", "club": "Jayachamaraja Wadiyar Golf Club", "member_name": "Darshanprasad S" }, { "member_id": "VM 0014", "club": "Jayachamaraja Wadiyar Golf Club", "member_name": "Varsha M" }] };

  scores: any = []//[{ "h_8": "3", "h_7": "4", "h_9": "5", "h_10": "4", "h_11": "4", "h_12": "3", "description": "PAR", "h_13": "4", "h_14": "5", "h_15": "3", "h_16": "4", "h_in": "36", "h_17": "4", "h_18": "5", "slno": 1, "h_out": "34", "total": "70", "h_2": "3", "h_1": "4", "h_4": "4", "net": "70", "h_3": "4", "h_6": "3", "h_5": "4" }, { "h_8": "13", "h_7": "9", "h_9": "3", "h_10": "14", "h_11": "10", "h_12": "18", "description": "STROKE INDEX", "h_13": "12", "h_14": "2", "h_15": "16", "h_16": "6", "h_in": "null", "h_17": "4", "h_18": "8", "slno": 2, "h_out": "null", "total": "null", "h_2": "17", "h_1": "5", "h_4": "7", "net": "null", "h_3": "11", "h_6": "15", "h_5": "1" }]

  holeNo = ["description", "h_1", "h_2", "h_3", "h_4", "h_5", "h_6", "h_7", "h_8", "h_9", "h_out", "h_10", "h_11", "h_12", "h_13", "h_14", "h_15", "h_16", "h_17", "h_18", "h_in", "total"];

  scoreBoard1 = ["Hole No", "1", "2", "3", "4", "5", "6", "7", "8", "9", "out", "10", "11", "12", "13", "14", "15", "16", "17", "18", "in", "total"];



  constructor(public navCtrl: NavController, public navParams: NavParams, public common: CommonService, public storage: StorageService, public platform: Platform) {
    this.platform.registerBackButtonAction((event) => {
      console.log(event);
      this.common.Alert.confirm("Do you want to exit?", "Scoreboard", "", "Yes", "No").then(res => {
        if (res) {
          this.common.saveAlert().then(res => {
            if (res == 1) {
              this.saveScore();
            }else{
              this.submitScore();
            }
          })
        }
      })
    });

    for (var cnt = 1; cnt <= 18; cnt++) {
      this.holes.push('H' + cnt);
    }

    this.loadUnsavedScores().then(data => {
      if (data) {
        this.common.Alert.confirm("Do you want to contine with unsaved scores?", "Scoreboard", "", "Yes", "Discard").then(res => {
          if (res) {
            this.totalScoreCard = JSON.parse(data[0].scores);
          }
          this.common.loaderShow();
          this.getSlotDetails();
        })
      } else {
        this.common.loaderShow();
        this.getSlotDetails();
      }
    })

  }

  loadUnsavedScores() {
    return new Promise(resolve => {
      this.storage.querySelect("SELECT scores from tblscores WHERE key_name= ? AND id= ?", ['score', this.common.authData.member_id]).then(data => {
        console.log(data);
        if (data && data.length > 0) {
          resolve(data);
        } else {
          resolve(false);
        }
      });
    })
  }

  getSlotDetails() {
    let params = "?tag=getCurrentSlot&booked_member_id=" + this.common.authData.member_id;
    this.common.postService(params).then((res: any) => {
      if (res.error && res.error == 1) {
        this.common.presentToast("There is no slots are reserved.");
        setTimeout(() => {
          this.common.loaderHide();
          this.navCtrl.setRoot(HomePage);
        }, 2000);
      } else {
        this.currSlot = res;
        this.common.loaderHide();

        this.getParDetails().then(() => {
          this.manageScores();
        }).catch(err => {
          this.common.loaderHide();
          console.log(err);
        })
      }
    })
  }

  getParDetails() {
    return new Promise((resolve, reject) => {
      let params = "?tag=getPar";
      this.common.postService(params).then((res: any) => {
        this.scores = res;
        resolve();
      }).catch(err => reject(err))
    })
  }

  manageScores() {
    this.currRow = this.scores.length + 1;
    this.currSlot[this.currSlot.slot_id].forEach(val => {
      this.golfers.push({ golfer: val.member_name, mem_id: val.member_id, club: val.club, handicap: 0 });
    });
    this.golfers.push({ golfer: this.currSlot.booked_member_name, id: this.currSlot.booked_member_id, club: this.currSlot.club, handicap: this.currSlot.hc });

    this.golfers.forEach(val => {
      this.players += val.golfer + ', ';
    });
    this.players = this.players.slice(0, this.players.length - 3);

    if (this.totalScoreCard.length == 0) {
      let tempScores = [this.scoreBoard1];

      this.scores.forEach((value, key) => {
        let tempArr = [];
        for (let i = 0; i < 22; i++) {
          tempArr.push(value[this.holeNo[i]] == "null" ? '' : value[this.holeNo[i]]);
        }
        tempScores.push(tempArr);
      });

      this.golfers.forEach(golr => {
        let tempArr = [];
        for (let i = 0; i < 22; i++) {
          tempArr.push(i == 0 ? golr.golfer : 0);
        }
        tempScores.push(tempArr);
      })
      console.log(tempScores);
      this.totalScoreCard = tempScores;
    }
  }


  ionViewDidLoad() {

  }

  scoreEnter(type) {
    if (type == 1) {
      if (this.score > 0) {
        this.score--;
        let scoreItr = 1;
        if (parseInt(this.currHole) >= 9) {
          scoreItr = 2;
        }
        this.totalScoreCard[parseInt(this.currGolfer) + (this.scores.length + 1)][10] = 0;
        this.totalScoreCard[parseInt(this.currGolfer) + (this.scores.length + 1)][20] = 0;

        this.totalScoreCard[parseInt(this.currGolfer) + (this.scores.length + 1)][parseInt(this.currHole) + scoreItr] = this.score;

        this.totalScoreCard[parseInt(this.currGolfer) + (this.scores.length + 1)].forEach((value, key) => {
          if (key > 0 && key <= 9) {
            this.totalScoreCard[parseInt(this.currGolfer) + (this.scores.length + 1)][10] = parseInt(this.totalScoreCard[parseInt(this.currGolfer) + (this.scores.length + 1)][10]) + value;
          } else if (key > 10 && key < 20) {
            this.totalScoreCard[parseInt(this.currGolfer) + (this.scores.length + 1)][20] = parseInt(this.totalScoreCard[parseInt(this.currGolfer) + (this.scores.length + 1)][20]) + value;
          }
        })
        this.totalScoreCard[parseInt(this.currGolfer) + (this.scores.length + 1)][21] = this.totalScoreCard[parseInt(this.currGolfer) + (this.scores.length + 1)][10] + this.totalScoreCard[parseInt(this.currGolfer) + (this.scores.length + 1)][20];
      }
    } else {
      if (this.score < 9) {
        this.score++;
        let scoreItr = 1;
        if (parseInt(this.currHole) >= 9) {
          scoreItr = 2;
        }
        this.totalScoreCard[parseInt(this.currGolfer) + (this.scores.length + 1)][10] = 0;
        this.totalScoreCard[parseInt(this.currGolfer) + (this.scores.length + 1)][20] = 0;
        this.totalScoreCard[parseInt(this.currGolfer) + (this.scores.length + 1)][parseInt(this.currHole) + scoreItr] = this.score;
        this.totalScoreCard[parseInt(this.currGolfer) + (this.scores.length + 1)].forEach((value, key) => {
          if (key > 0 && key <= 9) {
            this.totalScoreCard[parseInt(this.currGolfer) + (this.scores.length + 1)][10] = parseInt(this.totalScoreCard[parseInt(this.currGolfer) + (this.scores.length + 1)][10]) + value;
          } else if (key > 10 && key < 20) {
            this.totalScoreCard[parseInt(this.currGolfer) + (this.scores.length + 1)][20] = parseInt(this.totalScoreCard[parseInt(this.currGolfer) + (this.scores.length + 1)][20]) + value;
          }
        })
        this.totalScoreCard[parseInt(this.currGolfer) + (this.scores.length + 1)][21] = this.totalScoreCard[parseInt(this.currGolfer) + (this.scores.length + 1)][10] + this.totalScoreCard[parseInt(this.currGolfer) + (this.scores.length + 1)][20];
      }
    }
  }

  changeHoleGolfer(param, type) {
    if (type == 1) {
      this.currRow = parseInt(param) + (this.scores.length + 1);
      console.log(this.currRow);
    } else {
      this.currCol = parseInt(param) + 1;
      console.log(this.currCol);
    }
  }

  submitScore() {
    this.finalScore = [];
    let aggreScores = [];
    this.totalScoreCard.forEach((scores, key) => {
      let temp: any = JSON.parse(JSON.stringify(scores));
      if (key > 2) {
        temp.splice(0, 1);
        temp.splice(9, 1);
        temp.splice(18, 2);
        aggreScores.push(temp);
      }
    })

    console.log(aggreScores);
    this.golfers.forEach((golfr, key) => {
      aggreScores[key].forEach((aggr, itr) => {
        golfr['h_' + (itr + 1)] = aggr;
      })
    })
    var params = "?tag=saveScore&slot_id=" + this.currSlot.slot_id + "&scoreDetails=" + JSON.stringify(this.golfers);
    this.common.postService(params).then(res => {
      if (res > 0) {
        this.common.presentToast('Scores Submitted Successfully');
        setTimeout(() => {
          this.navCtrl.setRoot(HomePage);
        }, 2000);
      } else {
        this.common.presentToast('Oops. Something went wrong');
      }
    })
    console.log(this.golfers);
  }

  saveScore() {
    this.storage.querySelect("SELECT * from tblscores WHERE key_name= ?", ['score']).then(data => {
      if (data && data.length > 0) {
        this.storage.query("UPDATE tblscores SET scores = ? WHERE key_name = ? AND id = ?", [JSON.stringify(this.totalScoreCard), 'score', this.common.authData.member_id])
      } else {
        this.storage.query("INSERT INTO tblscores(key_name,scores,id) VALUES(?,?,?)", ['score', JSON.stringify(this.totalScoreCard), this.common.authData.member_id]);
      }
      this.common.presentToast("Your score was saved successfully");
    })
  }

}
