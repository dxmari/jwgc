import { CommonService } from './../../app/services/global.service';
import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";

@IonicPage({
  name: "accounts",
  segment: "accounts"
})
@Component({
  selector: "page-accounts",
  templateUrl: "accounts.html"
})
export class AccountsPage {
  accountsList = [
    {
      member_id: "NK 0011",
      debit_on: "2018-06-01",
      account_id: 1,
      debit_at: "3.35 PM",
      club: "JAYACHAMARAJA WADIYAR GOLF CLUB",
      tour_fee: 1000,
      tournament: "AIRFOCE CUP-2018",
      member_name: "NAVEEN KUMAR K M",
      tour_year: "2018"
    },
    {
      member_id: "NK 0021",
      debit_on: "2018-06-01",
      account_id: 1,
      debit_at: "3.35 PM",
      club: "JAYACHAMARAJA WADIYAR GOLF CLUB",
      tour_fee: 1000,
      tournament: "AIRFOCE CUP-2018",
      member_name: "NAVEEN KUMAR K M",
      tour_year: "2018"
    },
    {
      member_id: "NK 0041",
      debit_on: "2018-06-01",
      account_id: 1,
      debit_at: "3.35 PM",
      club: "JAYACHAMARAJA WADIYAR GOLF CLUB",
      tour_fee: 1000,
      tournament: "AIRFOCE CUP-2018",
      member_name: "NAVEEN KUMAR K M",
      tour_year: "2018"
    }
  ];

  constructor(public navCtrl: NavController, public navParams: NavParams, public common: CommonService) {}

  ionViewDidLoad() {
    var params = '?tag=getAccountStatement&member_id=' + this.common.authData.member_id;
    this.common.postService(params).then((res: any) => {
      // this.accountsList = res;
      console.log(this.accountsList);
    })
  }
}
