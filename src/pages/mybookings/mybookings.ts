import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CommonService } from '../../app/services/index';

@IonicPage({
  name: 'mybookings',
  segment: 'mybookings',
})
@Component({
  selector: 'page-mybookings',
  templateUrl: 'mybookings.html',
})
export class MybookingsPage {
  bookingList = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, public common: CommonService) {
  }

  ionViewDidLoad() {
    var params = "?tag=getSlots&member_id=" + this.common.authData.member_id;
    this.common.postService(params).then((res: any) => {
      console.log(res);
      this.bookingList = res;
    })
    console.log('ionViewDidLoad MybookingsPage');
  }

}
