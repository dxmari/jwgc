import { CommonService } from './../../app/services/global.service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import * as $ from './../../assets/js/jquery.min.js';


@IonicPage({
  name: 'slot-booking',
  segment: 'slot-booking'
})
@Component({
  selector: 'page-slot-booking',
  templateUrl: 'slot-booking.html',
})
export class SlotBookingPage {
  search1: string;
  search2: string;
  search3: string;
  isShow = false;
  isClicked = false;
  results = [];
  isLoading: boolean;

  golfer1: any = {};
  golfer2: any = {};
  golfer3: any = {};

  golferFlg1 = false;
  golferFlg2 = false;
  golferFlg3 = false;

  urlParams: any = {};

  constructor(public navCtrl: NavController, public navParams: NavParams, public common: CommonService) {
    this.urlParams = this.navParams.get('params');
    console.log(this.urlParams)
  }

  ionViewDidLoad() {
    this.getMembers();
    console.log('ionViewDidLoad SlotBookingPage');
    $('input').attr('autocomplete', 'off');
    var $input = $('.form-fieldset > input');

    $input.blur(function (e) {
      // $('label span').css('color','rgba(255, 255, 255, .2)')
      $(this).toggleClass('filled', !!$(this).val());
    });
  }

  getMembers() {
    this.common.loaderShow();
    var params = "?tag=getMembers&member_name=";
    this.common.postService(params).then((res: any) => {
      this.common.loaderHide();
      this.results = res;
    }).catch(err => {
      this.common.loaderHide();
      console.log(err);
    })
  }

  selectItem(e) {
    this.isClicked = true;
    this.isShow = false;
    console.log('clicked')
  }

  openModal(type, e) {
    this.common.modalOpen('pageselect', { options: this.results }).then(res => {
      if (res) {
        if (type == 1) {
          this.golfer1 = res;
        } else if (type == 2) {
          this.golfer2 = res;
        } else if (type == 3) {
          this.golfer3 = res;
        }
        console.log(res);
      }
    })
  }

  closeAutoComplete(){

  }

  reserveSlot() {
    var params = '?tag=saveSlot';
    var members = [];
    if (this.golfer1.member_id) {
      this.golferFlg1 = false;
      members.push(this.golfer1);
    } else {
      this.golferFlg1 = true;
    }
    if (this.golfer2.member_id) {
      members.push(this.golfer2);
      this.golferFlg2 = false;
    } else {
      this.golferFlg2 = true;
    }
    if (this.golfer3.member_id) {
      members.push(this.golfer3);
      this.golferFlg3 = false;
    } else {
      this.golferFlg3 = true;
    }
    // var isDuplicate = false;
    // members.forEach(val => {
    //   let idx = members.findIndex((e) => {
    //     return e.member_id == val.member_id;
    //   });
    //   console.log(idx);
    //   if (idx >= 0) {
    //     isDuplicate = true;
    //   }
    // });
    // if (isDuplicate) {
    //   this.common.presentToast('Please select different Golfers')
    //   return;
    // }
    var user = this.common.authData;
    params += '&booked_member_id=' + user.member_id;
    params += '&booked_member_name=' + user.member_name;
    params += '&club=' + user.club;
    params += '&slot_date=' + this.urlParams.date;
    params += '&slot_time=' + this.common.dateFormat(this.urlParams.time,'HH:mm:ss');
    params += '&golfers=' + JSON.stringify(members);

    if (members.length < 3) {
      return;
    }
    console.log(params);
    this.common.loaderShow();
    this.common.postService(params).then((res: any) => {
      this.golfer1 = '';
      this.golfer2 = '';
      this.golfer3 = '';
      this.common.loaderHide();
      if (res.error == 0) {
        this.common.presentToast('Slot Reserved Successfully');
      }
    }).catch(err => {
      this.common.loaderHide();
      console.log(err);
    })
  }
}
