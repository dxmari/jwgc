import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { CommonService } from '../../app/services/index';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {

  constructor(public navCtrl: NavController, public common: CommonService) {

  }

  goPage(type) {
    let pageNm;
    if (type == 1) {
      pageNm = 'pre-slot';
    } else if (type == '2') {
      pageNm = 'tournaments';
    } else if (type == '3') {
      pageNm = 'rewards';
    } else if (type == '4') {
      pageNm = 'scores';
    } else if (type == '5') {
      pageNm = 'accounts';
    } else {
      pageNm = 'notifications';
    }
    this.navCtrl.push(pageNm);
  }

  gobookings() {
    this.navCtrl.push('mybookings');
  }

}
