import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PreSlotBookingPage } from './pre-slot-booking';

@NgModule({
  declarations: [
    PreSlotBookingPage,
  ],
  imports: [
    IonicPageModule.forChild(PreSlotBookingPage),
  ],
})
export class PreSlotBookingPageModule {}
