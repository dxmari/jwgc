import { CommonService } from "./../../app/services/global.service";
import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";

@IonicPage({
  name: "pre-slot",
  segment: "pre-slot"
})
@Component({
  selector: "page-pre-slot-booking",
  templateUrl: "pre-slot-booking.html"
})
export class PreSlotBookingPage {
  myDate;
  slotTimings = [];
  type = 2;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public common: CommonService
  ) { }

  ionViewDidLoad() {
    this.slotTimings = [];
    this.loadTimings("tomorrow");
  }

  loadTimings(myDate) {
    console.log(myDate);
    if (myDate == "today") {
      this.type = 1;
      this.myDate = new Date().toJSON();
    } else {
      this.type = 2;
      var currentDate = new Date();
      currentDate.setDate(currentDate.getDate() + 1);
      this.myDate = new Date(currentDate).toJSON();
    }
    var params = "?tag=getSlotTime&slot_date=" + this.myDate;
    var mytime = new Date().toTimeString().split(" ")[0];
    // .slice(0, new Date().toTimeString().split(" ")[0].length - 3);
    this.common.postService(params).then((res: any) => {
      res.forEach(val => {
        val.time = new Date(
          new Date().toJSON().split("T")[0] + "T" + val.slot_time
        );
      });
      console.log(res);
      this.slotTimings = res;
      if (myDate == "today") {
        this.slotTimings = this.slotTimings.filter(value => {
          console.log(this.common.timeCompare(value.slot_time, mytime))
          return this.common.timeCompare(value.slot_time, mytime);
        });
      }
    });
  }

  goToBook(slot) {
    console.log(this.myDate)
    var params: any = {
      date: this.myDate.split("T")[0],
      time: slot.time.toJSON()
    };
    this.navCtrl.push("slot-booking", { params: params });
  }
}
