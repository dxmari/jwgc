import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage, ViewController } from 'ionic-angular';
import { CommonService } from './../../app/services/global.service';
@IonicPage({
  name: 'pageselect'
})
@Component({
  selector: 'page-model-select',
  templateUrl: 'model-select.html',
})
export class ModelSelectPage {
  options: any = [];
  searchItem = '';
  constructor(public navCtrl: NavController, public global: CommonService, public navParams: NavParams, public viewCtrl: ViewController) {
    this.options = JSON.parse(JSON.stringify(this.navParams.get('options')));
  }

  ionViewDidLoad() {
  }

  getOption(option) {
    this.viewCtrl.dismiss(option);
  }

  getMultipleOptions(option) {
    if (option.checked) {
      option.checked = false;
    } else {
      option.checked = true;
    }
  }

  submitOptions() {
    let options = [];
    options = this.options.filter((val) => {
      return val.checked == true;
    })
    this.options = JSON.parse(JSON.stringify(this.navParams.get('options')));
    this.viewCtrl.dismiss(options);
  }

  onInputGet(event: any) {

    this.options = this.navParams.get('options');
    let val;
    val = event.target.value;
    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.options = this.options.filter((item: any) => {
        if (item && item.member_name) {
          return (item.member_name.toLowerCase().indexOf(val.toLowerCase()) > -1);
        }
      })
    }
  }
  onCancel() {
    this.searchItem = '';
    this.options = JSON.parse(JSON.stringify(this.navParams.get('options')));
  }
}
