import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModelSelectPage } from './model-select';

@NgModule({
  declarations: [
    ModelSelectPage,
  ],
  imports: [
    IonicPageModule.forChild(ModelSelectPage),
  ],
})
export class ModelSelectPageModule {}
