import { CommonService } from './../../app/services/global.service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage({
  name: 'tournaments',
  segment: 'tournaments'
})
@Component({
  selector: 'page-tournaments',
  templateUrl: 'tournaments.html',
})
export class TournamentsPage {
  tournamentsList = [];
  imagUrl = '';
  constructor(public navCtrl: NavController, public navParams: NavParams, public common: CommonService) {
    this.imagUrl = common.IMGURL;
  }

  ionViewDidLoad() {
    var params = '?tag=getUpcomingTournaments&member_id=' + this.common.authData.member_name;
    this.common.postService(params).then((res: any) => {
      this.tournamentsList = res;
    })
  }

  bookTournament(slot) {
    console.log(slot);
    this.navCtrl.push('tournament-detail',{params:slot});
  }

}
