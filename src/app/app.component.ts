import { Component, ViewChild } from "@angular/core";
import { Platform, NavController } from "ionic-angular";
import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";

import { HomePage } from "../pages/home/home";
import { CommonService, StorageService } from "./services/index";
@Component({
  templateUrl: "app.html"
})
export class MyApp {
  rootPage: any = "";

  @ViewChild("nav") public nav: NavController;
  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    public common: CommonService,
    public storage: StorageService
  ) {
    platform.ready().then(() => {
      this.storage.initDB().then(() => {
        this.authenticate().then(() => {
          console.log("Authenticate Ends");
        });
      });
      statusBar.styleDefault();
      setTimeout(() => {
        splashScreen.hide();
      }, 100);
    });
  }
  signOut() {
    this.nav.setRoot("login");
  }

  ngAfterViewInit() {
    console.log(this.nav);
    this.nav.viewDidEnter.subscribe(view => {
      console.log("Current opened view is : " + view.name);
      this.common.currPage = view.name;
    });
  }

  authenticate() {
    console.log("Authenticate Starts");
    this.common.currPage = "login";
    return new Promise(resolve => {
      this.storage
        .fetchObject("auth")
        .then((response: any) => {
          if (Object.keys(response).length != 0 && response.member_id) {
            this.common.authData = response;
            this.storage.putObject("auth", response);
            this.rootPage = HomePage;
            // this.rootPage = 'scores';
          } else {
            this.rootPage = "login";
          }
          resolve();
        })
        .catch(err => {
          console.log(err + "----login");
        });
    })
  }
}
