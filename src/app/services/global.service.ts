import { Injectable } from "@angular/core";
import { Http, Headers } from "@angular/http";
import { Platform } from "ionic-angular";
import {
  PopoverController,
  ModalController,
  AlertController,
  LoadingController,
  ToastController,
  MenuController
} from "ionic-angular";
import "rxjs/add/operator/map";
import { StorageService } from "./index";
import { DatePipe } from "@angular/common";

@Injectable()
export class CommonService {
  authData: any = {};
  fcmToken = "";
  rootData = {};
  loading;
  currPage = "";
  labelAttribute = "name";
  // labelAttribute = "member_name";
  APIURL = "http://122.166.171.3:8080/jwgc/service.htm";
  IMGURL = "http://122.166.171.3:8080/jwgc/jwgcimages/";
  DOWNLDURL = "http://122.166.171.3:8080/jwgc/Download/";
  constructor(
    public http: Http,
    public platform: Platform,
    public storage: StorageService,
    public popupCtrl: PopoverController,
    public menuCtrl: MenuController,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController
  ) {
    this.init();
    this.authData = JSON.parse(localStorage.getItem("auth"));
  }

  init() {}

  loaderShow() {
    this.loading = this.loadingCtrl.create({
      spinner: "bubbles"
    });
    this.loading.present();
  }

  loaderHide() {
    this.loading.dismiss();
  }

  saveAlert(){
    return new Promise((resolve, reject) => {
      let alert = this.alertCtrl.create({
        title: "Scoreboard",
        message: "Do you want to Save the scores?",
        buttons: [
          {
            text: "Discard",
            role: "cancel",
            handler: () => {
              resolve(0);
            }
          },
          {
            text: "Save Later",
            handler: () => {
              resolve(1);
            }
          },
          {
            text: "Save Now",
            handler: () => {
              resolve(2);
            }
          }
        ]
      });
      alert.present();
    });
  }

  public Alert = {
    confirm: (msg?, title?, subTitle?,yesbtnText?,nobtnText?) => {
      return new Promise((resolve, reject) => {
        let alert = this.alertCtrl.create({
          title: title || "Confirm",
          subTitle: subTitle || "",
          message: msg || "Do you want continue?",
          buttons: [
            {
              text: nobtnText || "Cancel",
              role: "cancel",
              handler: () => {
                resolve(false);
              }
            },
            {
              text: yesbtnText || "Ok",
              handler: () => {
                resolve(true);
              }
            }
          ]
        });
        alert.present();
      });
    },
    alert: (msg, title?, subTitle?) => {
      let alert = this.alertCtrl.create({
        title: title || "Alert",
        subTitle: subTitle || "",
        message: msg,
        buttons: ["Dismiss"]
      });
      alert.present();
    }
  };

  setRootData() {
    return new Promise((resolve, reject) => {
      this.storage.fetchObject("rootData").then(data => {
        if (Object.keys(data).length !== 0) {
          this.rootData = data;
          resolve(this.rootData);
        } else {
          reject(this.rootData);
        }
      });
    });
  }

  postService(params?) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append("Content-Type", "application/json");

      this.http.post(this.APIURL + params, {}, { headers: headers }).subscribe(
        (res: any) => {
          resolve(res._body ? res.json() : res);
        },
        (err: any) => {
          reject(err._body ? err.json() : err);
        }
      );
    });
  }

  getService(url: string, token?: string) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      if (token) {
        headers.append("Authorization", "bearer " + token);
      }
      this.http.get(url, { headers: headers }).subscribe(
        (res: any) => {
          resolve(res._body ? res.json() : res);
        },
        (err: any) => {
          reject(err._body ? err.json() : err);
        }
      );
    });
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 4000,
      position: "bottom",
      dismissOnPageChange: true
    });

    toast.onDidDismiss(() => {
      console.log("Dismissed toast");
    });

    toast.present();
  }

  ionicPopup(commponent, data: any = {}) {
    let popup = this.popupCtrl.create(commponent, data);
    popup.present();
    return new Promise((resolve, reject) => {
      popup.onDidDismiss(data => {
        if (data) {
          resolve(data);
        } else {
          resolve(false);
        }
      });
    });
  }

  modalOpen(pageName: string, params: any) {
    return new Promise((resolve, reject) => {
      let addmodal = this.modalCtrl.create(pageName, params);
      addmodal.onDidDismiss(data => {
        if (data) {
          resolve(data);
        } else {
          resolve(false);
        }
      });
      addmodal.present();
    });
  }

  logout() {
    this.authData = "";
    localStorage.clear();
    this.storage.query("DROP TABLE auth");
    window.location.reload();
  }

  dateCompare(date1, date2) {
    var dt1, dt2;
    dt1 = typeof date1 == "string" ? new Date(date1) : date1;
    dt2 = typeof date2 == "string" ? new Date(date2) : date2;
    if (dt1.getFullYear() > dt2.getFullYear()) {
      return 1;
    } else if (dt1.getFullYear() < dt2.getFullYear()) {
      return -1;
    } else {
      if (dt1.getMonth() > dt2.getMonth()) {
        return 1;
      } else if (dt1.getMonth() < dt2.getMonth()) {
        return -1;
      } else {
        if (dt1.getDay() > dt2.getDay()) {
          return 1;
        } else if (dt1.getDay() < dt2.getDay()) {
          return -1;
        } else {
          return 0;
        }
      }
    }
  }

  dateFormat(date,format="yyyy-MM-dd") {
    var datepipe = new DatePipe('en-IN');
    return datepipe.transform(date, format);
  }

  timeCompare(endTime,startTime) {
    var regExp = /(\d{1,2})\:(\d{1,2})\:(\d{1,2})/;
    if (
      parseInt(endTime.replace(regExp, "$1$2$3")) >
      parseInt(startTime.replace(regExp, "$1$2$3"))
    ) {
      return true;
    } else {
      return false;
    }
  }
}
