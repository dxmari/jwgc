webpackJsonp([2],{

/***/ 298:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TournamentDetailPageModule", function() { return TournamentDetailPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tournament_detail__ = __webpack_require__(309);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var TournamentDetailPageModule = /** @class */ (function () {
    function TournamentDetailPageModule() {
    }
    TournamentDetailPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__tournament_detail__["a" /* TournamentDetailPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__tournament_detail__["a" /* TournamentDetailPage */]),
            ],
        })
    ], TournamentDetailPageModule);
    return TournamentDetailPageModule;
}());

//# sourceMappingURL=tournament-detail.module.js.map

/***/ }),

/***/ 309:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TournamentDetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_services_index__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_session_select_session_select__ = __webpack_require__(202);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TournamentDetailPage = /** @class */ (function () {
    function TournamentDetailPage(navCtrl, navParams, common) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.common = common;
        this.tournament = {};
        this.imagUrl = '';
        this.sessionList = [];
        this.imagUrl = common.IMGURL;
        this.tournament = navParams.get('params');
        console.log(this.tournament);
    }
    TournamentDetailPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log('ionViewDidLoad TournamentDetailPage');
        var params = '?tag=getSessions&tournament=' + this.tournament.tournament + '&tour_year=' + this.tournament.tour_year;
        this.common.postService(params).then(function (res) {
            _this.sessionList = res;
        });
        var params = '?tag=getRegistrationStatus&tournament=' + this.tournament.tournament + '&tour_year=' + this.tournament.tour_year + "&mem_id=" + this.common.authData.member_id;
        this.common.postService(params).then(function (res) {
            console.log(res);
            if (res.error == 1) {
            }
            else {
            }
        });
    };
    TournamentDetailPage.prototype.openRegister = function () {
        var _this = this;
        this.common.ionicPopup(__WEBPACK_IMPORTED_MODULE_3__components_session_select_session_select__["a" /* SessionSelectComponent */], { options: this.sessionList }).then(function (res) {
            if (res) {
                console.log(res);
                _this.bookTournament(res);
            }
        });
    };
    TournamentDetailPage.prototype.bookTournament = function (slot) {
        var _this = this;
        var params = "?tag=registerTournament&slno=" + slot.slno + "&mem_id=" + this.common.authData.member_id + "&isWaitingList=false&session_type=" + slot.session_name + "&session_date=" + slot.session_date + " &session_time=" + slot.session_time;
        this.common.postService(params).then(function (res) {
            console.log(res);
            if (res.error == 0) {
                _this.common.presentToast('Tournament Booked successfully');
            }
        });
    };
    TournamentDetailPage.prototype.cancelBook = function (slot) {
        var _this = this;
        var params = "?tag=unregisterTournament&slno=" + slot.slno + "&mem_id=" + this.common.authData.member_id;
        this.common.postService(params).then(function (res) {
            console.log(res);
            if (res.error == 0) {
                _this.common.presentToast('Tournament unregistered successfully.');
            }
        });
    };
    TournamentDetailPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-tournament-detail',template:/*ion-inline-start:"/Users/apple/Documents/ionic/jwdc/src/pages/tournament-detail/tournament-detail.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>{{tournament?.tournament}}</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <ion-card>\n    <ion-card-content>\n      <div class="avator">\n        <span class="img-span">\n          <!-- <img [src]="imagUrl + tournament?.image" alt=""> -->\n          <img [src]="imagUrl + \'1.png\'" alt="">\n        </span>\n      </div>\n      <br>\n      <div class="title">\n        <b>{{tournament?.tournament}}</b>\n      </div>\n      <div class="dates" *ngIf="tournament?.opening_date && tournament?.closing_date">\n        <b>Dates :</b>\n        <span>{{tournament?.opening_date | date : \'MMM dd yyyy\'}}</span> -\n        <span>{{tournament?.closing_date | date : \'MMM dd yyyy\'}}</span>\n      </div>\n      <div class="entryfee">\n        <b>Entry Fee :</b> Rs. {{tournament?.entryfee}} /-</div>\n    </ion-card-content>\n  </ion-card>\n\n  <ion-card>\n    <ion-card-header>\n      <span class="title">\n        <b>Available Sessions</b>\n      </span>\n    </ion-card-header>\n    <ion-card-content *ngFor="let session of sessionList">\n      <!-- <div class="avator">\n        <span class="title">\n          <b>Available Sessions</b>\n        </span>\n      </div> -->\n      <br>\n      <div class="title">\n        <b>{{session?.session_name}} {{session?.session_time}}</b>\n      </div>\n      <div class="dates">\n        <b>Session Date :</b>\n        <span>{{session?.session_date | date : \'MMM dd yyyy\'}}</span>\n      </div>\n    </ion-card-content>\n  </ion-card>\n</ion-content>\n\n<ion-footer padding>\n  <button (click)="openRegister()" ion-button class="btn btn-cls">Register</button>\n  <button ion-button class="btn btn-cls">Edit</button>\n</ion-footer>'/*ion-inline-end:"/Users/apple/Documents/ionic/jwdc/src/pages/tournament-detail/tournament-detail.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__app_services_index__["a" /* CommonService */]])
    ], TournamentDetailPage);
    return TournamentDetailPage;
}());

//# sourceMappingURL=tournament-detail.js.map

/***/ })

});
//# sourceMappingURL=2.js.map