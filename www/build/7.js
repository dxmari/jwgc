webpackJsonp([7],{

/***/ 293:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MybookingsPageModule", function() { return MybookingsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__mybookings__ = __webpack_require__(304);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var MybookingsPageModule = /** @class */ (function () {
    function MybookingsPageModule() {
    }
    MybookingsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__mybookings__["a" /* MybookingsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__mybookings__["a" /* MybookingsPage */]),
            ],
        })
    ], MybookingsPageModule);
    return MybookingsPageModule;
}());

//# sourceMappingURL=mybookings.module.js.map

/***/ }),

/***/ 304:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MybookingsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_services_index__ = __webpack_require__(42);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var MybookingsPage = /** @class */ (function () {
    function MybookingsPage(navCtrl, navParams, common) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.common = common;
        this.bookingList = [];
    }
    MybookingsPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        var params = "?tag=getSlots&member_id=" + this.common.authData.member_id;
        this.common.postService(params).then(function (res) {
            console.log(res);
            _this.bookingList = res;
        });
        console.log('ionViewDidLoad MybookingsPage');
    };
    MybookingsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-mybookings',template:/*ion-inline-start:"/Users/apple/Documents/ionic/jwdc/src/pages/mybookings/mybookings.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>My Bookings</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <ion-card *ngFor="let item of bookingList">\n    <ion-card-content>\n      <div class="padcls">{{item.club}}</div>\n      <span class="padcls">{{item.booked_member_name}}</span>\n      <span class="date">{{item.slot_date +\' \'+ item.slot_time}}</span>\n      <br>\n      <br>\n      <span class="padcls2" *ngFor="let mem of item[item.slot_id]">\n        {{mem.member_name}},\n      </span>\n    </ion-card-content>\n  </ion-card>\n  <ion-card *ngIf="bookingList.length==0">\n    <ion-card-content>\n      <div class="padcls center">There is no bookings.</div>\n    </ion-card-content>\n  </ion-card>\n</ion-content>'/*ion-inline-end:"/Users/apple/Documents/ionic/jwdc/src/pages/mybookings/mybookings.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__app_services_index__["a" /* CommonService */]])
    ], MybookingsPage);
    return MybookingsPage;
}());

//# sourceMappingURL=mybookings.js.map

/***/ })

});
//# sourceMappingURL=7.js.map