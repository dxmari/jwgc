webpackJsonp([5],{

/***/ 294:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PreSlotBookingPageModule", function() { return PreSlotBookingPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pre_slot_booking__ = __webpack_require__(305);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PreSlotBookingPageModule = /** @class */ (function () {
    function PreSlotBookingPageModule() {
    }
    PreSlotBookingPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__pre_slot_booking__["a" /* PreSlotBookingPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__pre_slot_booking__["a" /* PreSlotBookingPage */]),
            ],
        })
    ], PreSlotBookingPageModule);
    return PreSlotBookingPageModule;
}());

//# sourceMappingURL=pre-slot-booking.module.js.map

/***/ }),

/***/ 305:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PreSlotBookingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__app_services_global_service__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(29);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PreSlotBookingPage = /** @class */ (function () {
    function PreSlotBookingPage(navCtrl, navParams, common) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.common = common;
        this.slotTimings = [];
        this.type = 2;
    }
    PreSlotBookingPage.prototype.ionViewDidLoad = function () {
        this.slotTimings = [];
        this.loadTimings("tomorrow");
    };
    PreSlotBookingPage.prototype.loadTimings = function (myDate) {
        var _this = this;
        console.log(myDate);
        if (myDate == "today") {
            this.type = 1;
            this.myDate = new Date().toJSON();
        }
        else {
            this.type = 2;
            var currentDate = new Date();
            currentDate.setDate(currentDate.getDate() + 1);
            this.myDate = new Date(currentDate).toJSON();
        }
        var params = "?tag=getSlotTime&slot_date=" + this.myDate;
        var mytime = new Date().toTimeString().split(" ")[0];
        // .slice(0, new Date().toTimeString().split(" ")[0].length - 3);
        this.common.postService(params).then(function (res) {
            res.forEach(function (val) {
                val.time = new Date(new Date().toJSON().split("T")[0] + "T" + val.slot_time);
            });
            console.log(res);
            _this.slotTimings = res;
            if (myDate == "today") {
                _this.slotTimings = _this.slotTimings.filter(function (value) {
                    console.log(_this.common.timeCompare(value.slot_time, mytime));
                    return _this.common.timeCompare(value.slot_time, mytime);
                });
            }
        });
    };
    PreSlotBookingPage.prototype.goToBook = function (slot) {
        console.log(this.myDate);
        var params = {
            date: this.myDate.split("T")[0],
            time: slot.time.toJSON()
        };
        this.navCtrl.push("slot-booking", { params: params });
    };
    PreSlotBookingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: "page-pre-slot-booking",template:/*ion-inline-start:"/Users/apple/Documents/ionic/jwdc/src/pages/pre-slot-booking/pre-slot-booking.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Slot Booking</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <!-- <ion-item>\n    <ion-label>April, 2018</ion-label>\n    <ion-datetime (ngModelChange)="loadTimings($event)" displayFormat="MM/DD/YYYY" [(ngModel)]="myDate"></ion-datetime>\n  </ion-item> -->\n  <div class="top-bar">\n    <div class="center datecls">April, 2018</div>\n    <div class="btn-group-cls">\n      <button [class.active-cls]="type == 1" (click)="loadTimings(\'today\')" class="btns btn-cls br1">\n        <span>Today</span>\n        <br>\n        <span>Monday</span>\n      </button>\n      <button [class.active-cls]="type == 2" (click)="loadTimings(\'tomorrow\')" class="btns btn-cls">\n        <span>Tomorrow</span>\n        <br>\n        <span>Tuesday</span>\n      </button>\n    </div>\n  </div>\n  <ul class="slotul">\n    <h4 class="center">Slot Timings</h4>\n    <!-- slot.slot_time | date : \'hh:mm a\' -->\n    <button *ngFor="let slot of slotTimings" (click)="goToBook(slot)" class="btn btn-outline-secondary slotbooked" typ\n      e="button">{{slot.time | date : \'hh:mm a\'}}</button>\n  </ul>\n</ion-content>'/*ion-inline-end:"/Users/apple/Documents/ionic/jwdc/src/pages/pre-slot-booking/pre-slot-booking.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_0__app_services_global_service__["a" /* CommonService */]])
    ], PreSlotBookingPage);
    return PreSlotBookingPage;
}());

//# sourceMappingURL=pre-slot-booking.js.map

/***/ })

});
//# sourceMappingURL=5.js.map