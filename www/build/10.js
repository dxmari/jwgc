webpackJsonp([10],{

/***/ 290:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountsPageModule", function() { return AccountsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__accounts__ = __webpack_require__(301);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AccountsPageModule = /** @class */ (function () {
    function AccountsPageModule() {
    }
    AccountsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__accounts__["a" /* AccountsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__accounts__["a" /* AccountsPage */]),
            ],
        })
    ], AccountsPageModule);
    return AccountsPageModule;
}());

//# sourceMappingURL=accounts.module.js.map

/***/ }),

/***/ 301:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AccountsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__app_services_global_service__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(29);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AccountsPage = /** @class */ (function () {
    function AccountsPage(navCtrl, navParams, common) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.common = common;
        this.accountsList = [
            {
                member_id: "NK 0011",
                debit_on: "2018-06-01",
                account_id: 1,
                debit_at: "3.35 PM",
                club: "JAYACHAMARAJA WADIYAR GOLF CLUB",
                tour_fee: 1000,
                tournament: "AIRFOCE CUP-2018",
                member_name: "NAVEEN KUMAR K M",
                tour_year: "2018"
            },
            {
                member_id: "NK 0021",
                debit_on: "2018-06-01",
                account_id: 1,
                debit_at: "3.35 PM",
                club: "JAYACHAMARAJA WADIYAR GOLF CLUB",
                tour_fee: 1000,
                tournament: "AIRFOCE CUP-2018",
                member_name: "NAVEEN KUMAR K M",
                tour_year: "2018"
            },
            {
                member_id: "NK 0041",
                debit_on: "2018-06-01",
                account_id: 1,
                debit_at: "3.35 PM",
                club: "JAYACHAMARAJA WADIYAR GOLF CLUB",
                tour_fee: 1000,
                tournament: "AIRFOCE CUP-2018",
                member_name: "NAVEEN KUMAR K M",
                tour_year: "2018"
            }
        ];
    }
    AccountsPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        var params = '?tag=getAccountStatement&member_id=' + this.common.authData.member_id;
        this.common.postService(params).then(function (res) {
            // this.accountsList = res;
            console.log(_this.accountsList);
        });
    };
    AccountsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: "page-accounts",template:/*ion-inline-start:"/Users/apple/Documents/ionic/jwdc/src/pages/accounts/accounts.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Accounts</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <div class="card-cls" *ngFor="let item of accountsList">\n    <div class="card-bg bg-cls"></div>\n    <div class="card-content">\n      <div class="title">{{item.member_name}}</div>\n      <div class="title">{{item.tournament}}, {{item.tour_year}}</div>\n      <div class="title">Tournament Fee: {{item.tour_fee}}</div>\n      <div class="title">{{item.club}}</div>\n      <div class="date">{{item.debit_on + \' \' + item.debit_at}}</div>\n    </div>\n  </div>\n  <ion-card *ngIf="accountsList.length==0">\n    <ion-card-content>\n      <div class="padcls center">There is no data available.</div>\n    </ion-card-content>\n  </ion-card>\n</ion-content>'/*ion-inline-end:"/Users/apple/Documents/ionic/jwdc/src/pages/accounts/accounts.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_0__app_services_global_service__["a" /* CommonService */]])
    ], AccountsPage);
    return AccountsPage;
}());

//# sourceMappingURL=accounts.js.map

/***/ })

});
//# sourceMappingURL=10.js.map