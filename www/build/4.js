webpackJsonp([4],{

/***/ 296:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RewardsPageModule", function() { return RewardsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__rewards__ = __webpack_require__(307);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var RewardsPageModule = /** @class */ (function () {
    function RewardsPageModule() {
    }
    RewardsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__rewards__["a" /* RewardsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__rewards__["a" /* RewardsPage */]),
            ],
        })
    ], RewardsPageModule);
    return RewardsPageModule;
}());

//# sourceMappingURL=rewards.module.js.map

/***/ }),

/***/ 307:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RewardsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_services_global_service__ = __webpack_require__(200);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var RewardsPage = /** @class */ (function () {
    function RewardsPage(navCtrl, navParams, common) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.common = common;
        this.tournamentsList = [];
        this.imagUrl = '';
        this.imagUrl = common.IMGURL;
    }
    RewardsPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log('ionViewDidLoad RewardsPage');
        var params = '?tag=getCompletedTournaments';
        this.common.postService(params).then(function (res) {
            _this.tournamentsList = res;
        });
    };
    RewardsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-rewards',template:/*ion-inline-start:"/Users/apple/Documents/ionic/jwdc/src/pages/rewards/rewards.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>Rewards</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <div class="bg-apply"></div>\n  <div class="wrapper">\n    <div class="cols-cls">\n      <div class="col" *ngFor="let item of tournamentsList;let index=index;" ontouchstart="this.classList.toggle(\'hover\');">\n        <div class="container-cls">\n          <div class="front" style="background-image: url(\'assets/imgs/gulf1.jpg\')">\n            <div class="inner">\n              <p>{{item.tournament}}</p>\n              Club Name:&nbsp;&nbsp;\n              <span>{{item.club}}</span>\n              <br> Course Name:&nbsp;&nbsp;\n              <span>{{item.course}}</span>\n              <br>\n              <span>Entry Fee: &nbsp;&nbsp; {{item.entryfee}}</span>\n              <br>\n              <span>{{item.tour_date | date : \'dd MMM yyyy\'}}</span>\n            </div>\n          </div>\n          <div class="back">\n            <!-- <span class="slot_cls">{{item.slno}}</span> -->\n            <div class="inner">\n              <div class="inner-content">\n                <div class="first">\n                  <span class="img-span">\n                    <img [src]="imagUrl + item.image" alt="">\n                  </span>\n                </div>\n                <div class="second">\n                  <p *ngIf="item.sponsor != \'null\'">\n                    {{(!item.sponsor || item.sponsor == \'null\') ? \'\' : item.sponsor}}\n                  </p>\n                  <span>Rounds: &nbsp;&nbsp; {{item.noofrounds}}</span>\n                  <br>\n                  <span>Handicap for Male: &nbsp;&nbsp; {{item.maxhcformale}}</span>\n                  <br>\n                  <span>Handicap for Female: &nbsp;&nbsp; {{item.maxhcforfemale}}</span>\n                  <br>\n                </div>\n              </div>\n              <!-- <button ion-button class="btn">{{item.registerationStatus ? \'Cancel\' : \'Book\'}}</button> -->\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/apple/Documents/ionic/jwdc/src/pages/rewards/rewards.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__app_services_global_service__["a" /* CommonService */]])
    ], RewardsPage);
    return RewardsPage;
}());

//# sourceMappingURL=rewards.js.map

/***/ })

});
//# sourceMappingURL=4.js.map