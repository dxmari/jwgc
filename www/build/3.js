webpackJsonp([3],{

/***/ 297:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ScoreCardPageModule", function() { return ScoreCardPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__score_card__ = __webpack_require__(308);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ScoreCardPageModule = /** @class */ (function () {
    function ScoreCardPageModule() {
    }
    ScoreCardPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__score_card__["a" /* ScoreCardPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__score_card__["a" /* ScoreCardPage */]),
            ],
        })
    ], ScoreCardPageModule);
    return ScoreCardPageModule;
}());

//# sourceMappingURL=score-card.module.js.map

/***/ }),

/***/ 308:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ScoreCardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_services_storage_service__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_services_global_service__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ScoreCardPage = /** @class */ (function () {
    function ScoreCardPage(navCtrl, navParams, common, storage, platform) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.common = common;
        this.storage = storage;
        this.platform = platform;
        this.totalScoreCard = [];
        this.score = 0;
        this.currGolfer = "0";
        this.currHole = "0";
        this.golfers = [];
        this.holes = [];
        this.finalScore = [];
        this.players = '';
        this.currCol = 1;
        this.currSlot = {}; //{ "booked_member_name": "Naveen Kumar K M", "slot_id": 64, "club": "Jayachamaraja Wadiyar Golf Club", "hc": 10, "error": 0, "booked_member_id": "NK 0011", "64": [{ "member_id": "TJ 0015", "club": "Jayachamaraja Wadiyar Golf Club", "member_name": "Tejaswini" }, { "member_id": "DS 0012", "club": "Jayachamaraja Wadiyar Golf Club", "member_name": "Darshanprasad S" }, { "member_id": "VM 0014", "club": "Jayachamaraja Wadiyar Golf Club", "member_name": "Varsha M" }] };
        this.scores = []; //[{ "h_8": "3", "h_7": "4", "h_9": "5", "h_10": "4", "h_11": "4", "h_12": "3", "description": "PAR", "h_13": "4", "h_14": "5", "h_15": "3", "h_16": "4", "h_in": "36", "h_17": "4", "h_18": "5", "slno": 1, "h_out": "34", "total": "70", "h_2": "3", "h_1": "4", "h_4": "4", "net": "70", "h_3": "4", "h_6": "3", "h_5": "4" }, { "h_8": "13", "h_7": "9", "h_9": "3", "h_10": "14", "h_11": "10", "h_12": "18", "description": "STROKE INDEX", "h_13": "12", "h_14": "2", "h_15": "16", "h_16": "6", "h_in": "null", "h_17": "4", "h_18": "8", "slno": 2, "h_out": "null", "total": "null", "h_2": "17", "h_1": "5", "h_4": "7", "net": "null", "h_3": "11", "h_6": "15", "h_5": "1" }]
        this.holeNo = ["description", "h_1", "h_2", "h_3", "h_4", "h_5", "h_6", "h_7", "h_8", "h_9", "h_out", "h_10", "h_11", "h_12", "h_13", "h_14", "h_15", "h_16", "h_17", "h_18", "h_in", "total"];
        this.scoreBoard1 = ["Hole No", "1", "2", "3", "4", "5", "6", "7", "8", "9", "out", "10", "11", "12", "13", "14", "15", "16", "17", "18", "in", "total"];
        this.platform.registerBackButtonAction(function (event) {
            console.log(event);
            _this.common.Alert.confirm("Do you want to exit?", "Scoreboard", "", "Yes", "No").then(function (res) {
                if (res) {
                    _this.common.saveAlert().then(function (res) {
                        if (res == 1) {
                            _this.saveScore();
                        }
                        else {
                            _this.submitScore();
                        }
                    });
                }
            });
        });
        for (var cnt = 1; cnt <= 18; cnt++) {
            this.holes.push('H' + cnt);
        }
        this.loadUnsavedScores().then(function (data) {
            if (data) {
                _this.common.Alert.confirm("Do you want to contine with unsaved scores?", "Scoreboard", "", "Yes", "Discard").then(function (res) {
                    if (res) {
                        _this.totalScoreCard = JSON.parse(data[0].scores);
                    }
                    _this.common.loaderShow();
                    _this.getSlotDetails();
                });
            }
            else {
                _this.common.loaderShow();
                _this.getSlotDetails();
            }
        });
    }
    ScoreCardPage.prototype.loadUnsavedScores = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.storage.querySelect("SELECT scores from tblscores WHERE key_name= ? AND id= ?", ['score', _this.common.authData.member_id]).then(function (data) {
                console.log(data);
                if (data && data.length > 0) {
                    resolve(data);
                }
                else {
                    resolve(false);
                }
            });
        });
    };
    ScoreCardPage.prototype.getSlotDetails = function () {
        var _this = this;
        var params = "?tag=getCurrentSlot&booked_member_id=" + this.common.authData.member_id;
        this.common.postService(params).then(function (res) {
            if (res.error && res.error == 1) {
                _this.common.presentToast("There is no slots are reserved.");
                setTimeout(function () {
                    _this.common.loaderHide();
                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
                }, 2000);
            }
            else {
                _this.currSlot = res;
                _this.common.loaderHide();
                _this.getParDetails().then(function () {
                    _this.manageScores();
                }).catch(function (err) {
                    _this.common.loaderHide();
                    console.log(err);
                });
            }
        });
    };
    ScoreCardPage.prototype.getParDetails = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var params = "?tag=getPar";
            _this.common.postService(params).then(function (res) {
                _this.scores = res;
                resolve();
            }).catch(function (err) { return reject(err); });
        });
    };
    ScoreCardPage.prototype.manageScores = function () {
        var _this = this;
        this.currRow = this.scores.length + 1;
        this.currSlot[this.currSlot.slot_id].forEach(function (val) {
            _this.golfers.push({ golfer: val.member_name, mem_id: val.member_id, club: val.club, handicap: 0 });
        });
        this.golfers.push({ golfer: this.currSlot.booked_member_name, id: this.currSlot.booked_member_id, club: this.currSlot.club, handicap: this.currSlot.hc });
        this.golfers.forEach(function (val) {
            _this.players += val.golfer + ', ';
        });
        this.players = this.players.slice(0, this.players.length - 3);
        if (this.totalScoreCard.length == 0) {
            var tempScores_1 = [this.scoreBoard1];
            this.scores.forEach(function (value, key) {
                var tempArr = [];
                for (var i = 0; i < 22; i++) {
                    tempArr.push(value[_this.holeNo[i]] == "null" ? '' : value[_this.holeNo[i]]);
                }
                tempScores_1.push(tempArr);
            });
            this.golfers.forEach(function (golr) {
                var tempArr = [];
                for (var i = 0; i < 22; i++) {
                    tempArr.push(i == 0 ? golr.golfer : 0);
                }
                tempScores_1.push(tempArr);
            });
            console.log(tempScores_1);
            this.totalScoreCard = tempScores_1;
        }
    };
    ScoreCardPage.prototype.ionViewDidLoad = function () {
    };
    ScoreCardPage.prototype.scoreEnter = function (type) {
        var _this = this;
        if (type == 1) {
            if (this.score > 0) {
                this.score--;
                var scoreItr = 1;
                if (parseInt(this.currHole) >= 9) {
                    scoreItr = 2;
                }
                this.totalScoreCard[parseInt(this.currGolfer) + (this.scores.length + 1)][10] = 0;
                this.totalScoreCard[parseInt(this.currGolfer) + (this.scores.length + 1)][20] = 0;
                this.totalScoreCard[parseInt(this.currGolfer) + (this.scores.length + 1)][parseInt(this.currHole) + scoreItr] = this.score;
                this.totalScoreCard[parseInt(this.currGolfer) + (this.scores.length + 1)].forEach(function (value, key) {
                    if (key > 0 && key <= 9) {
                        _this.totalScoreCard[parseInt(_this.currGolfer) + (_this.scores.length + 1)][10] = parseInt(_this.totalScoreCard[parseInt(_this.currGolfer) + (_this.scores.length + 1)][10]) + value;
                    }
                    else if (key > 10 && key < 20) {
                        _this.totalScoreCard[parseInt(_this.currGolfer) + (_this.scores.length + 1)][20] = parseInt(_this.totalScoreCard[parseInt(_this.currGolfer) + (_this.scores.length + 1)][20]) + value;
                    }
                });
                this.totalScoreCard[parseInt(this.currGolfer) + (this.scores.length + 1)][21] = this.totalScoreCard[parseInt(this.currGolfer) + (this.scores.length + 1)][10] + this.totalScoreCard[parseInt(this.currGolfer) + (this.scores.length + 1)][20];
            }
        }
        else {
            if (this.score < 9) {
                this.score++;
                var scoreItr = 1;
                if (parseInt(this.currHole) >= 9) {
                    scoreItr = 2;
                }
                this.totalScoreCard[parseInt(this.currGolfer) + (this.scores.length + 1)][10] = 0;
                this.totalScoreCard[parseInt(this.currGolfer) + (this.scores.length + 1)][20] = 0;
                this.totalScoreCard[parseInt(this.currGolfer) + (this.scores.length + 1)][parseInt(this.currHole) + scoreItr] = this.score;
                this.totalScoreCard[parseInt(this.currGolfer) + (this.scores.length + 1)].forEach(function (value, key) {
                    if (key > 0 && key <= 9) {
                        _this.totalScoreCard[parseInt(_this.currGolfer) + (_this.scores.length + 1)][10] = parseInt(_this.totalScoreCard[parseInt(_this.currGolfer) + (_this.scores.length + 1)][10]) + value;
                    }
                    else if (key > 10 && key < 20) {
                        _this.totalScoreCard[parseInt(_this.currGolfer) + (_this.scores.length + 1)][20] = parseInt(_this.totalScoreCard[parseInt(_this.currGolfer) + (_this.scores.length + 1)][20]) + value;
                    }
                });
                this.totalScoreCard[parseInt(this.currGolfer) + (this.scores.length + 1)][21] = this.totalScoreCard[parseInt(this.currGolfer) + (this.scores.length + 1)][10] + this.totalScoreCard[parseInt(this.currGolfer) + (this.scores.length + 1)][20];
            }
        }
    };
    ScoreCardPage.prototype.changeHoleGolfer = function (param, type) {
        if (type == 1) {
            this.currRow = parseInt(param) + (this.scores.length + 1);
            console.log(this.currRow);
        }
        else {
            this.currCol = parseInt(param) + 1;
            console.log(this.currCol);
        }
    };
    ScoreCardPage.prototype.submitScore = function () {
        var _this = this;
        this.finalScore = [];
        var aggreScores = [];
        this.totalScoreCard.forEach(function (scores, key) {
            var temp = JSON.parse(JSON.stringify(scores));
            if (key > 2) {
                temp.splice(0, 1);
                temp.splice(9, 1);
                temp.splice(18, 2);
                aggreScores.push(temp);
            }
        });
        console.log(aggreScores);
        this.golfers.forEach(function (golfr, key) {
            aggreScores[key].forEach(function (aggr, itr) {
                golfr['h_' + (itr + 1)] = aggr;
            });
        });
        var params = "?tag=saveScore&slot_id=" + this.currSlot.slot_id + "&scoreDetails=" + JSON.stringify(this.golfers);
        this.common.postService(params).then(function (res) {
            if (res > 0) {
                _this.common.presentToast('Scores Submitted Successfully');
                setTimeout(function () {
                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
                }, 2000);
            }
            else {
                _this.common.presentToast('Oops. Something went wrong');
            }
        });
        console.log(this.golfers);
    };
    ScoreCardPage.prototype.saveScore = function () {
        var _this = this;
        this.storage.querySelect("SELECT * from tblscores WHERE key_name= ?", ['score']).then(function (data) {
            if (data && data.length > 0) {
                _this.storage.query("UPDATE tblscores SET scores = ? WHERE key_name = ? AND id = ?", [JSON.stringify(_this.totalScoreCard), 'score', _this.common.authData.member_id]);
            }
            else {
                _this.storage.query("INSERT INTO tblscores(key_name,scores,id) VALUES(?,?,?)", ['score', JSON.stringify(_this.totalScoreCard), _this.common.authData.member_id]);
            }
            _this.common.presentToast("Your score was saved successfully");
        });
    };
    ScoreCardPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_4__angular_core__["m" /* Component */])({
            selector: 'page-score-card',template:/*ion-inline-start:"/Users/apple/Documents/ionic/jwdc/src/pages/score-card/score-card.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>Score Card</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <ion-card>\n    <ion-card-content>\n      <div class="title"><b>Golfers</b></div>\n      <span class="golfrs">{{players}}</span>&nbsp;\n      <!-- <span class="golfrs">Mari</span>,&nbsp;\n      <span class="golfrs">Mari</span>,&nbsp;\n      <span class="golfrs">Mari</span> -->\n    </ion-card-content>\n  </ion-card>\n  <div class="table-cls">\n    <table>\n      <thead>\n        <tr>\n          <th colspan="22">Score Card</th>\n        </tr>\n      </thead>\n      <tbody>\n        <tr class="{{currRow == index1 ? \'bgapply\' : \'\'}}" *ngFor="let score of totalScoreCard;let index1=index;">\n          <td class="{{(currCol == index2 && currRow == index1) ? \'tdbg\' : \'\'}}" *ngFor="let item of score;let index2=index;">{{item}}</td>\n        </tr>\n      </tbody>\n    </table>\n  </div>\n  <ion-card>\n    <ion-card-content>\n      <div class="enter-score">\n        <h4 class="sr-cls">Enter the Scores</h4>\n        <div class="select-div">\n          <label class="lblcls">Golfers</label>\n          <select class="select-cls" name="golfer" (ngModelChange)="changeHoleGolfer($event,1)"  [(ngModel)]="currGolfer">\n            <option *ngFor="let opt of golfers;let index3 = index" value="{{index3}}">{{opt.golfer}}</option>\n          </select>\n        </div>\n        <div class="select-div">\n          <label class="lblcls">Hole Name</label>\n          <select class="select-cls" name="hole" (ngModelChange)="changeHoleGolfer($event,2)" [(ngModel)]="currHole">\n            <option *ngFor="let opt of holes;let index4 = index" value="{{index4}}">{{opt}}</option>\n          </select>\n        </div>\n        <div class="select-div">\n          <label class="lblcls">Score</label>\n          <div class="score-add">\n            <div class="score-incre">\n              <ion-icon (click)="scoreEnter(1)" class="icon-cls" name="md-remove"></ion-icon>\n              <input type="text" readonly [(ngModel)]="score" name="score" class="input-cls">\n              <ion-icon (click)="scoreEnter(2)" class="icon-cls" name="md-add"></ion-icon>\n            </div>\n          </div>\n        </div>\n      </div>\n    </ion-card-content>\n  </ion-card> \n</ion-content>\n\n<ion-footer>\n  <button ion-button (click)="saveScore()" class="footrbtn fl">Save</button>\n  <button ion-button (click)="submitScore()" class="footrbtn fr">Sumbit</button>\n</ion-footer>'/*ion-inline-end:"/Users/apple/Documents/ionic/jwdc/src/pages/score-card/score-card.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__app_services_global_service__["a" /* CommonService */], __WEBPACK_IMPORTED_MODULE_1__app_services_storage_service__["a" /* StorageService */], __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["k" /* Platform */]])
    ], ScoreCardPage);
    return ScoreCardPage;
}());

//# sourceMappingURL=score-card.js.map

/***/ })

});
//# sourceMappingURL=3.js.map