webpackJsonp([9],{

/***/ 291:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login__ = __webpack_require__(302);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var LoginPageModule = /** @class */ (function () {
    function LoginPageModule() {
    }
    LoginPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */]),
            ],
        })
    ], LoginPageModule);
    return LoginPageModule;
}());

//# sourceMappingURL=login.module.js.map

/***/ }),

/***/ 302:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__home_home__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_services_index__ = __webpack_require__(42);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, navParams, common, storage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.common = common;
        this.storage = storage;
        this.loginFlg = true;
        this.otp = '';
        this.logoutFlg = 'hide';
        this.otpFlg = false;
        this.memFlg = false;
        this.mobilFlg = false;
        this.response = {};
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    LoginPage.prototype.login = function (userid, mobile) {
        var _this = this;
        if (userid && mobile) {
            this.mobilFlg = false;
            this.memFlg = false;
            var params = '?tag=login&member_id=' + userid + '&mobile_no=' + mobile;
            this.common.postService(params).then(function (res) {
                console.log(res);
                if (res.error == 0) {
                    _this.loginFlg = false;
                    _this.otp = res.OTP;
                    _this.response = res;
                }
                else {
                    _this.common.presentToast('Oops, Something went wrong.');
                }
            }).catch(function (err) {
                console.log(err);
            });
        }
        else {
            if (!userid) {
                this.memFlg = true;
            }
            ;
            if (!mobile) {
                this.mobilFlg = true;
            }
            ;
        }
    };
    LoginPage.prototype.checkOTP = function (otp) {
        if (otp) {
            if (this.otp == otp) {
                this.otpFlg = false;
                this.common.authData = this.response;
                localStorage.setItem('auth', JSON.stringify(this.response));
                this.storage.setObject('auth', this.response);
                this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_0__home_home__["a" /* HomePage */]);
            }
            else {
                this.common.presentToast('Invalid OTP or expired');
            }
        }
        else {
            if (!otp)
                this.otpFlg = true;
        }
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"/Users/apple/Documents/ionic/jwdc/src/pages/login/login.html"*/'<ion-content padding class="login-content">\n  <div class="lc-block toggled" id="l-login">\n    <div align="center" style="margin: 10px;">\n      <img src="assets/imgs/logo-tips.jpg">\n    </div>\n\n    <div class="input-group m-b-20" *ngIf="loginFlg">\n      <span class="input-group-addon">\n        <i class="md md-person"></i>\n      </span>\n\n      <div class="fg-line">\n        <input [(ngModel)]="userId" type="text" class="form-control" placeholder="Member ID" value="" id="userName">\n        <span *ngIf="memFlg" class="err">Member ID is required</span>\n      </div>\n    </div>\n\n    <div class="input-group m-b-20" *ngIf="loginFlg">\n      <span class="input-group-addon">\n        <i class="md md-lock"></i>\n      </span>\n\n      <div class="fg-line">\n        <input [(ngModel)]="mobile" type="text" class="form-control" placeholder="Mobile Number" value="" id="password">\n        <span *ngIf="mobilFlg" class="err">Mobile no is required</span>\n      </div>\n    </div>\n\n    <div class="input-group m-b-20" *ngIf="!loginFlg">\n      <span class="input-group-addon">\n        <i class="md md-lock"></i>\n      </span>\n\n      <div class="fg-line">\n        <input type="text" [(ngModel)]="enteredOtp" class="form-control" placeholder="Enter OTP" value="" id="otp">\n        <span *ngIf="otpFlg" class="err">OTP is required</span>\n      </div>\n    </div>\n\n    <a *ngIf="loginFlg" (click)="login(userId,mobile)" class="btn btn-login btn-danger btn-float">\n      <i class="md md-arrow-forward"></i>\n    </a>\n    <a *ngIf="!loginFlg" (click)="checkOTP(enteredOtp)" class="btn btn-login btn-danger btn-float">\n      <i class="md md-arrow-forward"></i>\n    </a>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/apple/Documents/ionic/jwdc/src/pages/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__app_services_index__["a" /* CommonService */], __WEBPACK_IMPORTED_MODULE_3__app_services_index__["c" /* StorageService */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ })

});
//# sourceMappingURL=9.js.map