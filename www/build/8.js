webpackJsonp([8],{

/***/ 292:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModelSelectPageModule", function() { return ModelSelectPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__model_select__ = __webpack_require__(303);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ModelSelectPageModule = /** @class */ (function () {
    function ModelSelectPageModule() {
    }
    ModelSelectPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__model_select__["a" /* ModelSelectPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__model_select__["a" /* ModelSelectPage */]),
            ],
        })
    ], ModelSelectPageModule);
    return ModelSelectPageModule;
}());

//# sourceMappingURL=model-select.module.js.map

/***/ }),

/***/ 303:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModelSelectPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_services_global_service__ = __webpack_require__(200);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ModelSelectPage = /** @class */ (function () {
    function ModelSelectPage(navCtrl, global, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.global = global;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.options = [];
        this.searchItem = '';
        this.options = JSON.parse(JSON.stringify(this.navParams.get('options')));
    }
    ModelSelectPage.prototype.ionViewDidLoad = function () {
    };
    ModelSelectPage.prototype.getOption = function (option) {
        this.viewCtrl.dismiss(option);
    };
    ModelSelectPage.prototype.getMultipleOptions = function (option) {
        if (option.checked) {
            option.checked = false;
        }
        else {
            option.checked = true;
        }
    };
    ModelSelectPage.prototype.submitOptions = function () {
        var options = [];
        options = this.options.filter(function (val) {
            return val.checked == true;
        });
        this.options = JSON.parse(JSON.stringify(this.navParams.get('options')));
        this.viewCtrl.dismiss(options);
    };
    ModelSelectPage.prototype.onInputGet = function (event) {
        this.options = this.navParams.get('options');
        var val;
        val = event.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.options = this.options.filter(function (item) {
                if (item && item.member_name) {
                    return (item.member_name.toLowerCase().indexOf(val.toLowerCase()) > -1);
                }
            });
        }
    };
    ModelSelectPage.prototype.onCancel = function () {
        this.searchItem = '';
        this.options = JSON.parse(JSON.stringify(this.navParams.get('options')));
    };
    ModelSelectPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-model-select',template:/*ion-inline-start:"/Users/apple/Documents/ionic/jwdc/src/pages/model-select/model-select.html"*/'<ion-header>\n\n  <ion-navbar class="custom-txtcolor">\n      <ion-buttons end>\n          <button ion-button color="light" (click)="viewCtrl.dismiss(\'\')"> \n            <ion-icon name="close"></ion-icon>\n          </button>\n      </ion-buttons>\n    <ion-title>Members</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n  <ion-toolbar color="light">\n    <ion-searchbar [(ngModel)]="searchItem" class="search-bar" color="danger" (ionInput)="onInputGet($event)"> </ion-searchbar>\n    <button ion-button outline class="clear-btn" (click)="onCancel()">Clear</button>\n  </ion-toolbar>\n\n  <ion-scroll scrollY="true">\n    <ion-list>\n        <div *ngFor="let option of options | slice:0:100">\n          <div class="multiselect" (click)="getOption(option)">\n            <ion-icon name="checkmark" [hidden]="!option.checked" class="check-cls"></ion-icon>\n            <div class="country_cls"> {{option.member_name}} </div>\n        </div>\n        </div>\n    </ion-list>\n  </ion-scroll>\n</ion-content>\n\n<!-- <ion-footer>\n    <button ion-button full class="cancel-btn" (click)="submitOptions()">Sumbit</button>\n</ion-footer> -->\n'/*ion-inline-end:"/Users/apple/Documents/ionic/jwdc/src/pages/model-select/model-select.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__app_services_global_service__["a" /* CommonService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */]])
    ], ModelSelectPage);
    return ModelSelectPage;
}());

//# sourceMappingURL=model-select.js.map

/***/ })

});
//# sourceMappingURL=8.js.map