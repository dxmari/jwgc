webpackJsonp([11],{

/***/ 102:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_services_index__ = __webpack_require__(42);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, common) {
        this.navCtrl = navCtrl;
        this.common = common;
    }
    HomePage.prototype.goPage = function (type) {
        var pageNm;
        if (type == 1) {
            pageNm = 'pre-slot';
        }
        else if (type == '2') {
            pageNm = 'tournaments';
        }
        else if (type == '3') {
            pageNm = 'rewards';
        }
        else if (type == '4') {
            pageNm = 'scores';
        }
        else if (type == '5') {
            pageNm = 'accounts';
        }
        else {
            pageNm = 'notifications';
        }
        this.navCtrl.push(pageNm);
    };
    HomePage.prototype.gobookings = function () {
        this.navCtrl.push('mybookings');
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/Users/apple/Documents/ionic/jwdc/src/pages/home/home.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      JWGC\n    </ion-title>\n    <!-- <ion-icon (click)="common.logout()" class="logout" name="power"></ion-icon> -->\n  </ion-navbar>\n</ion-header>\n\n<ion-content class="bg">\n  <div class="wrapper">\n    <h3 class="wel_cls">Welcome {{common?.authData?.member_name}}</h3>\n    <div class="cols-cls">\n      <div class="col" ontouchstart="this.classList.toggle(\'hover\');">\n        <div class="container-cls">\n          <div class="front" style="background-image: url(\'assets/imgs/gulf1.jpg\')">\n            <div class="inner">\n              <p>Slot Booking</p>\n              <span></span>\n            </div>\n          </div>\n          <div class="back">\n            <div class="inner">\n              <button ion-button (click)="goPage(1)" class="btn">Book Slot</button>\n              <button ion-button (click)="gobookings()" class="btn">My Bookings</button>\n              <!-- <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit.\n              </p> -->\n            </div>\n          </div>\n        </div>\n      </div>\n      <div class="col" ontouchstart="this.classList.toggle(\'hover\');">\n        <div class="container-cls">\n          <div class="front" style="background-image: url(\'assets/imgs/gulf2.jpg\')">\n            <div class="inner">\n              <p>Tournament</p>\n              <span></span>\n            </div>\n          </div>\n          <div class="back">\n            <div class="inner" (click)="goPage(2)">\n              <p>Click here to book your tournaments\n              </p>\n            </div>\n          </div>\n        </div>\n      </div>\n      <br>\n      <div class="col" ontouchstart="this.classList.toggle(\'hover\');">\n        <div class="container-cls">\n          <div class="front" style="background-image: url(\'assets/imgs/gulf3.jpg\')">\n            <div class="inner">\n              <p>Rewards</p>\n              <span></span>\n            </div>\n          </div>\n          <div class="back" (click)="goPage(3)">\n            <div class="inner">\n              <p>Click here to show completed tournaments\n              </p>\n            </div>\n          </div>\n        </div>\n      </div>\n      <div class="col" ontouchstart="this.classList.toggle(\'hover\');">\n        <div class="container-cls">\n          <div class="front" style="background-image: url(\'assets/imgs/gulf4.jpg\')">\n            <div class="inner">\n              <p>Score Card</p>\n              <span></span>\n            </div>\n          </div>\n          <div class="back">\n            <div class="inner" (click)="goPage(4)">\n              <p>Click here to Know your Scores\n              </p>\n            </div>\n          </div>\n        </div>\n      </div>\n      <div class="col" ontouchstart="this.classList.toggle(\'hover\');">\n        <div class="container-cls">\n          <div class="front" style="background-image: url(\'assets/imgs/gulf4.jpg\')">\n            <div class="inner">\n              <p>Accounts</p>\n              <span></span>\n            </div>\n          </div>\n          <div class="back">\n            <div class="inner" (click)="goPage(5)">\n              <p>Click here to view your accounts\n              </p>\n            </div>\n          </div>\n        </div>\n      </div>\n      <div class="col" ontouchstart="this.classList.toggle(\'hover\');">\n        <div class="container-cls">\n          <div class="front" style="background-image: url(\'assets/imgs/gulf4.jpg\')">\n            <div class="inner">\n              <p>Notifications</p>\n              <span></span>\n            </div>\n          </div>\n          <div class="back">\n            <div class="inner" (click)="goPage(6)">\n              <p>Click here to show your notifications\n              </p>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/apple/Documents/ionic/jwdc/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__app_services_index__["a" /* CommonService */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 112:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 112;

/***/ }),

/***/ 154:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/accounts/accounts.module": [
		290,
		10
	],
	"../pages/login/login.module": [
		291,
		9
	],
	"../pages/model-select/model-select.module": [
		292,
		8
	],
	"../pages/mybookings/mybookings.module": [
		293,
		7
	],
	"../pages/notifications/notifications.module": [
		295,
		6
	],
	"../pages/pre-slot-booking/pre-slot-booking.module": [
		294,
		5
	],
	"../pages/rewards/rewards.module": [
		296,
		4
	],
	"../pages/score-card/score-card.module": [
		297,
		3
	],
	"../pages/slot-booking/slot-booking.module": [
		299,
		0
	],
	"../pages/tournament-detail/tournament-detail.module": [
		298,
		2
	],
	"../pages/tournaments/tournaments.module": [
		300,
		1
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 154;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 200:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CommonService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__index__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_common__ = __webpack_require__(32);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var CommonService = /** @class */ (function () {
    function CommonService(http, platform, storage, popupCtrl, menuCtrl, modalCtrl, alertCtrl, loadingCtrl, toastCtrl) {
        var _this = this;
        this.http = http;
        this.platform = platform;
        this.storage = storage;
        this.popupCtrl = popupCtrl;
        this.menuCtrl = menuCtrl;
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.authData = {};
        this.fcmToken = "";
        this.rootData = {};
        this.currPage = "";
        this.labelAttribute = "name";
        // labelAttribute = "member_name";
        this.APIURL = "http://122.166.171.3:8080/jwgc/service.htm";
        this.IMGURL = "http://122.166.171.3:8080/jwgc/jwgcimages/";
        this.DOWNLDURL = "http://122.166.171.3:8080/jwgc/Download/";
        this.Alert = {
            confirm: function (msg, title, subTitle, yesbtnText, nobtnText) {
                return new Promise(function (resolve, reject) {
                    var alert = _this.alertCtrl.create({
                        title: title || "Confirm",
                        subTitle: subTitle || "",
                        message: msg || "Do you want continue?",
                        buttons: [
                            {
                                text: nobtnText || "Cancel",
                                role: "cancel",
                                handler: function () {
                                    resolve(false);
                                }
                            },
                            {
                                text: yesbtnText || "Ok",
                                handler: function () {
                                    resolve(true);
                                }
                            }
                        ]
                    });
                    alert.present();
                });
            },
            alert: function (msg, title, subTitle) {
                var alert = _this.alertCtrl.create({
                    title: title || "Alert",
                    subTitle: subTitle || "",
                    message: msg,
                    buttons: ["Dismiss"]
                });
                alert.present();
            }
        };
        this.init();
        this.authData = JSON.parse(localStorage.getItem("auth"));
    }
    CommonService.prototype.init = function () { };
    CommonService.prototype.loaderShow = function () {
        this.loading = this.loadingCtrl.create({
            spinner: "bubbles"
        });
        this.loading.present();
    };
    CommonService.prototype.loaderHide = function () {
        this.loading.dismiss();
    };
    CommonService.prototype.saveAlert = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var alert = _this.alertCtrl.create({
                title: "Scoreboard",
                message: "Do you want to Save the scores?",
                buttons: [
                    {
                        text: "Discard",
                        role: "cancel",
                        handler: function () {
                            resolve(0);
                        }
                    },
                    {
                        text: "Save Later",
                        handler: function () {
                            resolve(1);
                        }
                    },
                    {
                        text: "Save Now",
                        handler: function () {
                            resolve(2);
                        }
                    }
                ]
            });
            alert.present();
        });
    };
    CommonService.prototype.setRootData = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.fetchObject("rootData").then(function (data) {
                if (Object.keys(data).length !== 0) {
                    _this.rootData = data;
                    resolve(_this.rootData);
                }
                else {
                    reject(_this.rootData);
                }
            });
        });
    };
    CommonService.prototype.postService = function (params) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Content-Type", "application/json");
            _this.http.post(_this.APIURL + params, {}, { headers: headers }).subscribe(function (res) {
                resolve(res._body ? res.json() : res);
            }, function (err) {
                reject(err._body ? err.json() : err);
            });
        });
    };
    CommonService.prototype.getService = function (url, token) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            if (token) {
                headers.append("Authorization", "bearer " + token);
            }
            _this.http.get(url, { headers: headers }).subscribe(function (res) {
                resolve(res._body ? res.json() : res);
            }, function (err) {
                reject(err._body ? err.json() : err);
            });
        });
    };
    CommonService.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 4000,
            position: "bottom",
            dismissOnPageChange: true
        });
        toast.onDidDismiss(function () {
            console.log("Dismissed toast");
        });
        toast.present();
    };
    CommonService.prototype.ionicPopup = function (commponent, data) {
        if (data === void 0) { data = {}; }
        var popup = this.popupCtrl.create(commponent, data);
        popup.present();
        return new Promise(function (resolve, reject) {
            popup.onDidDismiss(function (data) {
                if (data) {
                    resolve(data);
                }
                else {
                    resolve(false);
                }
            });
        });
    };
    CommonService.prototype.modalOpen = function (pageName, params) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var addmodal = _this.modalCtrl.create(pageName, params);
            addmodal.onDidDismiss(function (data) {
                if (data) {
                    resolve(data);
                }
                else {
                    resolve(false);
                }
            });
            addmodal.present();
        });
    };
    CommonService.prototype.logout = function () {
        this.authData = "";
        localStorage.clear();
        this.storage.query("DROP TABLE auth");
        window.location.reload();
    };
    CommonService.prototype.dateCompare = function (date1, date2) {
        var dt1, dt2;
        dt1 = typeof date1 == "string" ? new Date(date1) : date1;
        dt2 = typeof date2 == "string" ? new Date(date2) : date2;
        if (dt1.getFullYear() > dt2.getFullYear()) {
            return 1;
        }
        else if (dt1.getFullYear() < dt2.getFullYear()) {
            return -1;
        }
        else {
            if (dt1.getMonth() > dt2.getMonth()) {
                return 1;
            }
            else if (dt1.getMonth() < dt2.getMonth()) {
                return -1;
            }
            else {
                if (dt1.getDay() > dt2.getDay()) {
                    return 1;
                }
                else if (dt1.getDay() < dt2.getDay()) {
                    return -1;
                }
                else {
                    return 0;
                }
            }
        }
    };
    CommonService.prototype.dateFormat = function (date, format) {
        if (format === void 0) { format = "yyyy-MM-dd"; }
        var datepipe = new __WEBPACK_IMPORTED_MODULE_5__angular_common__["d" /* DatePipe */]('en-IN');
        return datepipe.transform(date, format);
    };
    CommonService.prototype.timeCompare = function (endTime, startTime) {
        var regExp = /(\d{1,2})\:(\d{1,2})\:(\d{1,2})/;
        if (parseInt(endTime.replace(regExp, "$1$2$3")) >
            parseInt(startTime.replace(regExp, "$1$2$3"))) {
            return true;
        }
        else {
            return false;
        }
    };
    CommonService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_4__index__["c" /* StorageService */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* PopoverController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* ToastController */]])
    ], CommonService);
    return CommonService;
}());

//# sourceMappingURL=global.service.js.map

/***/ }),

/***/ 201:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StorageService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite__ = __webpack_require__(157);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DB_NAME = 'myDB';
var win = window;
var StorageService = /** @class */ (function () {
    function StorageService(platform, sqlite) {
        this.platform = platform;
        this.sqlite = sqlite;
        this.set = function (key, value) {
            window.localStorage[key] = value;
        };
        this.get = function (key, defaultValue) {
            return window.localStorage[key] || defaultValue;
        };
        this.setObject = function (key, value) {
            var _this = this;
            window.localStorage[key] = JSON.stringify(value);
            this.fetchObject(key).then(function (data) {
                if (data != undefined && Object.keys(data).length != 0) {
                    _this.query("UPDATE auth SET session_data = ? WHERE key_name = ?", [JSON.stringify(value), key]);
                }
                else {
                    _this.query("INSERT INTO auth(key_name,session_data) VALUES(?,?)", [key, JSON.stringify(value)]);
                }
            });
        };
        this.putObject = function (key, value) {
            window.localStorage[key] = JSON.stringify(value);
        };
        this.getObject = function (key) {
            var data = window.localStorage[key];
            if (typeof data == 'string' && data != 'undefined')
                data = JSON.parse(data);
            return (data || {});
        };
        this.remove = function (key) {
            window.localStorage[key] = undefined;
        };
        this.clear = function (key) {
            window.localStorage.clear();
        };
    }
    StorageService.prototype.fetchObject = function (key) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.querySelect("SELECT session_data from auth WHERE key_name= ?", [key])
                .then(function (result) {
                var data;
                if (result.length == 0)
                    data = {};
                else
                    data = JSON.parse(result[0].session_data);
                resolve(data);
            }, function (err) {
                reject("Authentication Failed. Please try again later");
            });
        });
    };
    StorageService.prototype.storeRootData = function (value) {
        this.setObject('rootData', value);
    };
    StorageService.prototype.initDB = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (_this.platform.is('cordova')) {
                _this.sqlite.create({
                    name: DB_NAME,
                    location: 'default' // the location field is required
                }).then(function (dbObj) {
                    _this._db = dbObj;
                    // this.createTables();
                    _this.query("CREATE TABLE IF NOT EXISTS auth (key_name text, session_data text)").then(function () {
                        console.log('Table created');
                        _this.query("CREATE TABLE IF NOT EXISTS tblscores (key_name text, scores text,id text)").then(function (data) {
                            console.log('Storage Initiated', data);
                        }).catch(function (err) {
                            console.error('Storage: Unable to create initial storage tables', err.tx, err.err);
                        });
                        resolve();
                    }).catch(function (err) {
                        reject(err);
                        console.log('Storage: Unable to create initial storage tables', err.tx, err.err);
                    });
                    console.log(_this._db);
                }).catch(function (err) {
                    reject(err);
                });
            }
            else {
                console.warn('Storage: SQLite plugin not installed, falling back to WebSQL. Make sure to install cordova-sqlite-storage in production!');
                _this._db = win.openDatabase(DB_NAME, '1.0', 'database', 5 * 1024 * 1024);
                _this.query("CREATE TABLE IF NOT EXISTS auth (key_name text, session_data text)").then(function () {
                    _this.query("CREATE TABLE IF NOT EXISTS tblscores (key_name text, scores text,id text)").then(function (data) {
                        console.log('Storage Initiated', data);
                    }).catch(function (err) {
                        console.error('Storage: Unable to create initial storage tables', err.tx, err.err);
                    });
                    resolve();
                }).catch(function (err) {
                    console.error('Storage: Unable to create initial storage tables', err.tx, err.err);
                    reject(err);
                });
            }
        });
    };
    StorageService.prototype.createTables = function () {
        // this.query("DROP TABLE rexho_auth")
        this.query("CREATE TABLE IF NOT EXISTS auth (key_name text, session_data text)").then(function (data) {
            console.error('Storage Initiated', data);
        }).catch(function (err) {
            console.error('Storage: Unable to create initial storage tables', err.tx, err.err);
        });
        console.log(this._db);
    };
    StorageService.prototype.query = function (query, params) {
        var _this = this;
        if (params === void 0) { params = []; }
        return new Promise(function (resolve, reject) {
            try {
                _this._db.transaction(function (tx) {
                    tx.executeSql(query, params, function (tx, res) { return resolve({ tx: tx, res: res }); }, function (tx, err) { return reject({ tx: tx, err: err }); });
                }, function (err) { return reject({ err: err }); });
            }
            catch (err) {
                reject({ err: err });
            }
        });
    };
    StorageService.prototype.querySelect = function (query, params) {
        if (params === void 0) { params = []; }
        return this.query(query, params).then(function (data) {
            var result = [];
            if (data.res.rows.length > 0) {
                for (var cnt = 0; cnt < data.res.rows.length; cnt++) {
                    result.push(data.res.rows.item(cnt));
                }
            }
            return result;
        });
    };
    StorageService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite__["a" /* SQLite */]])
    ], StorageService);
    return StorageService;
}());

//# sourceMappingURL=storage.service.js.map

/***/ }),

/***/ 202:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SessionSelectComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular_navigation_nav_controller__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular_navigation_nav_params__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular_navigation_view_controller__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SessionSelectComponent = /** @class */ (function () {
    function SessionSelectComponent(navCtrl, navParams, view) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.view = view;
        this.sessionList = [];
        this.sessionList = navParams.get('options');
        console.log(this.sessionList);
    }
    SessionSelectComponent.prototype.register = function (session) {
        this.view.dismiss(session);
    };
    SessionSelectComponent.prototype.cancel = function () {
        this.view.dismiss(false);
    };
    SessionSelectComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'session-select',template:/*ion-inline-start:"/Users/apple/Documents/ionic/jwdc/src/components/session-select/session-select.html"*/'<div class="title-bar">\n  Select Session\n</div>\n\n<div class="title-content">\n  <ion-list>\n    <div class="item-left" (click)="register(session)" *ngFor="let session of sessionList">\n      <span class="clr">{{session.session_name}} {{session.session_time}}</span>\n    </div>\n  </ion-list>\n  <div class="footer-btn">\n    <button (click)="cancel()" ion-button class="btn btn-cls">Cancel</button>\n  </div>\n</div>\n\n<!-- <ion-footer padding>\n  <button (click)="cancel()" ion-button class="btn btn-cls">Cancel</button>\n</ion-footer> -->'/*ion-inline-end:"/Users/apple/Documents/ionic/jwdc/src/components/session-select/session-select.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular_navigation_nav_controller__["a" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular_navigation_nav_params__["a" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular_navigation_view_controller__["a" /* ViewController */]])
    ], SessionSelectComponent);
    return SessionSelectComponent;
}());

//# sourceMappingURL=session-select.js.map

/***/ }),

/***/ 203:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(226);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 226:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_http__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_forms__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_sqlite__ = __webpack_require__(157);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_component__ = __webpack_require__(289);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_home_home__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__services_index__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__components_session_select_session_select__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__angular_common__ = __webpack_require__(32);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};













var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_9__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_11__components_session_select_session_select__["a" /* SessionSelectComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/accounts/accounts.module#AccountsPageModule', name: 'accounts', segment: 'accounts', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'login', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/model-select/model-select.module#ModelSelectPageModule', name: 'pageselect', segment: 'model-select', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/mybookings/mybookings.module#MybookingsPageModule', name: 'mybookings', segment: 'mybookings', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/pre-slot-booking/pre-slot-booking.module#PreSlotBookingPageModule', name: 'pre-slot', segment: 'pre-slot', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/notifications/notifications.module#NotificationsPageModule', name: 'notifications', segment: 'notifications', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/rewards/rewards.module#RewardsPageModule', name: 'rewards', segment: 'rewards', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/score-card/score-card.module#ScoreCardPageModule', name: 'scores', segment: 'scores', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/tournament-detail/tournament-detail.module#TournamentDetailPageModule', name: 'tournament-detail', segment: 'tournament-detail', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/slot-booking/slot-booking.module#SlotBookingPageModule', name: 'slot-booking', segment: 'slot-booking', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/tournaments/tournaments.module#TournamentsPageModule', name: 'tournaments', segment: 'tournaments', priority: 'low', defaultHistory: [] }
                    ]
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_9__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_11__components_session_select_session_select__["a" /* SessionSelectComponent */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_10__services_index__["c" /* StorageService */],
                __WEBPACK_IMPORTED_MODULE_10__services_index__["a" /* CommonService */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_sqlite__["a" /* SQLite */],
                __WEBPACK_IMPORTED_MODULE_12__angular_common__["d" /* DatePipe */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 258:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RelativeTime; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_date_fns_distance_in_words_to_now__ = __webpack_require__(259);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_date_fns_distance_in_words_to_now___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_date_fns_distance_in_words_to_now__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var RelativeTime = /** @class */ (function () {
    function RelativeTime() {
    }
    /**
     * Takes a value and makes it lowercase.
     */
    RelativeTime.prototype.transform = function (value) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        return __WEBPACK_IMPORTED_MODULE_1_date_fns_distance_in_words_to_now___default()(new Date(value), { addSuffix: true });
    };
    RelativeTime = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* Pipe */])({
            name: 'relativeTime',
        })
    ], RelativeTime);
    return RelativeTime;
}());

//# sourceMappingURL=relative-time.js.map

/***/ }),

/***/ 289:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_index__ = __webpack_require__(42);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, common, storage) {
        var _this = this;
        this.common = common;
        this.storage = storage;
        this.rootPage = "";
        platform.ready().then(function () {
            _this.storage.initDB().then(function () {
                _this.authenticate().then(function () {
                    console.log("Authenticate Ends");
                });
            });
            statusBar.styleDefault();
            setTimeout(function () {
                splashScreen.hide();
            }, 100);
        });
    }
    MyApp.prototype.signOut = function () {
        this.nav.setRoot("login");
    };
    MyApp.prototype.ngAfterViewInit = function () {
        var _this = this;
        console.log(this.nav);
        this.nav.viewDidEnter.subscribe(function (view) {
            console.log("Current opened view is : " + view.name);
            _this.common.currPage = view.name;
        });
    };
    MyApp.prototype.authenticate = function () {
        var _this = this;
        console.log("Authenticate Starts");
        this.common.currPage = "login";
        return new Promise(function (resolve) {
            _this.storage
                .fetchObject("auth")
                .then(function (response) {
                if (Object.keys(response).length != 0 && response.member_id) {
                    _this.common.authData = response;
                    _this.storage.putObject("auth", response);
                    _this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */];
                    // this.rootPage = 'scores';
                }
                else {
                    _this.rootPage = "login";
                }
                resolve();
            })
                .catch(function (err) {
                console.log(err + "----login");
            });
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("nav"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/apple/Documents/ionic/jwdc/src/app/app.html"*/'<!-- <ion-menu [content]="content" id="menu">\n    <ion-header>\n        <ion-toolbar color="secondary">\n            <ion-title>JWSC</ion-title>\n        </ion-toolbar>\n    </ion-header>\n\n    <ion-content class="bg-overlay">\n        <ion-list>\n            <button class="btn-cls" ion-item menuClose="menu" detail-none (click)="nav.push(\'slot-booking\')">\n                Slot Booking\n            </button>\n            <button class="btn-cls" ion-item menuClose="menu" detail-none (click)="nav.push(\'tournaments\')">\n                Tournaments\n            </button>\n            <button  class="btn-cls" ion-item menuClose="menu" detail-none (click)="nav.push(\'rewards\')">\n                Rewards\n            </button>\n            <button  class="btn-cls" ion-item menuClose="menu" detail-none (click)="nav.push(\'notifications\')">\n                Notifications\n            </button>\n            <button  class="btn-cls" ion-item menuClose="menu" detail-none (click)="signOut()">\n                Logout\n            </button>\n        </ion-list>\n    </ion-content>\n\n</ion-menu> -->\n<ion-icon *ngIf="common.currPage != \'LoginPage\'" (click)="common.logout()" class="logout" name="power"></ion-icon>\n<ion-nav #content #nav [root]="rootPage"></ion-nav>'/*ion-inline-end:"/Users/apple/Documents/ionic/jwdc/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_5__services_index__["a" /* CommonService */],
            __WEBPACK_IMPORTED_MODULE_5__services_index__["c" /* StorageService */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 42:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__storage_service__ = __webpack_require__(201);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_0__storage_service__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__global_service__ = __webpack_require__(200);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_1__global_service__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__relative_time__ = __webpack_require__(258);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_2__relative_time__["a"]; });



//# sourceMappingURL=index.js.map

/***/ })

},[203]);
//# sourceMappingURL=main.js.map